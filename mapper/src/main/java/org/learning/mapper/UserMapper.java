package org.learning.mapper;

import org.learning.domain.UserCommand;
import org.learning.entity.UserEntity;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserEntity userCommandToUser(UserCommand userCommand);

    UserCommand userToUserCommand(UserEntity userEntity);

    @AfterMapping
    default void setFullName(@MappingTarget UserEntity userEntity) {
        userEntity.setFullName(userEntity.getFirstName() + " " + userEntity.getLastName());
    }
}
