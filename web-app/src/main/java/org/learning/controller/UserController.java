package org.learning.controller;

import org.learning.domain.UserCommand;
import org.learning.entity.UserEntity;
import org.learning.mapper.UserMapper;

public class UserController {

    public UserEntity saveUser(UserCommand userCommand) {
        //busuness logic
        return UserMapper.INSTANCE.userCommandToUser(userCommand);
    }
}
