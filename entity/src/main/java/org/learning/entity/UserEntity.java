package org.learning.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class UserEntity {

    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String fullName;
    private String email;

}
